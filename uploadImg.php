<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

require 'vendor/autoload.php';

use Intervention\Image\ImageManagerStatic as Image;

// configure with favored image driver (gd by default)
Image::configure(array('driver' => 'gd'));

$infos = $_FILES['image'];
$img = Image::make($infos['tmp_name']);
$ext = explode('/', $img->mime())[1];

$path = './static/';

if (!is_dir($path)) {
    mkdir($path, 0775, true);
}

$name = md5(time()) . '.' . $ext;
$img->save($path . $name, 30);

echo json_encode([
    'url' => '/static/' . $name
]);