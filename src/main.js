// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ContentStore from './stores/ContentStore'
import Konsepsyon from './Konsepsyon'

Vue.config.productionTip = false

window.BASE_URL = 'http://localhost:8000/uploadImg.php'

if (!String.prototype.splice) {
    /**
     * {JSDoc}
     *
     * The splice() method changes the content of a string by removing a range of
     * characters and/or adding new characters.
     *
     * @this {String}
     * @param {number} start Index at which to start changing the string.
     * @param {number} delCount An integer indicating the number of old chars to remove.
     * @param {string} newSubStr The String that is spliced in.
     * @return {string} A new string with the spliced substring.
     */
    String.prototype.splice = function(start, delCount, newSubStr) {
        return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
    };
}

/* eslint-disable no-new */
new Vue({
	el: '#app',
  	components: { Konsepsyon },
  	store: ContentStore
})
