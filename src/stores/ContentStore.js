import Vuex from 'vuex'
import Vue from 'vue'

Vue.use( Vuex )

export default new Vuex.Store( {
    strict: true,
    
    state: {
        mode: 'desktop',
        background: '#FDFDFD',
        bkgForm: false,
        rte: {
            show: false,
            type: 'text',
            x: null,
            y: null,
            el: null,
            design: null
        },
        rows: []
    },
    
    getters: {
        rte( state ) {
            return state.rte
        },
        
        rows( state ) {
            return state.rows
        },
        
        background( state ) {
            return state.background
        },
        
        mode( state ) {
            return state.mode
        },
        
        bkgForm( state ) {
            return state.bkgForm
        }
    },
    
    mutations: {
        addRow( state, { row } ) {
            state.rows.push( row )
        },
        
        addRows( state, { rows } ) {
            rows.forEach( ( row ) => {
                this.commit( 'addRow', { row } )
            } )
        },
        
        removeRow( state, row ) {
            state.rows.splice( row, 1 )
        },
        
        copyRow( state, row ) {
            const toCopy = state.rows[ row ]
            
            state.rows.push( toCopy )
        },
        
        updateRow( state, { index, target, value } ) {
            state.rows[ index ].props[ target ] = value
        },
        
        reorderRows( state, { oldIndex, newIndex } ) {
            const movedItem = state.rows.find( ( item, index ) => index === oldIndex )
            const remainingItems = state.rows.filter( ( item, index ) => index !== oldIndex )
            
            state.rows = [
                ...remainingItems.slice( 0, newIndex ),
                movedItem,
                ...remainingItems.slice( newIndex )
            ]
        },
        
        updateColor( state, { index, color } ) {
            state.rows[ index ].appearance[ 'backgroundColor' ] = color
        },
        
        updateBackground( state, { index, img } ) {
            state.rows[ index ].appearance[ 'background' ] = 'transparent url("' + img + '") no-repeat center center'
            state.rows[ index ].appearance[ 'backgroundSize' ] = 'cover'
        },
        
        showRte( state, rte ) {
            state.rte = rte
        },
        
        changeBackground( state, value ) {
            state.background = value.hex
        },
        
        changeMode( state, value ) {
            state.mode = value
        },
        
        bkgForm( state, value ) {
            state.bkgForm = value
        }
    },
    
    actions: {
        loadRows( context ) {
            context.commit( 'addRows', {
                rows: [
                    {
                        type: 'TitleBloc',
                        props: {
                            title: 'Mon Super Titre'
                        },
                        appearance: {
                            width: '100%',
                            padding: '1em',
                            backgroundColor: '#fff',
                            maxWidth: '960px',
                            margin: '0 auto',
                            textAlign: 'center',
                            fontFamily: 'sans-serif'
                        }
                    },
                    {
                        type: 'ThreeColsBloc',
                        props: {
                            content1: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates vitae deserunt laborum adipisci veniam nemo quas perspiciatis officia! Velit odio ipsa illum molestiae laborum tenetur magni pariatur iure vero natus.',
                            content2: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates vitae deserunt laborum adipisci veniam nemo quas perspiciatis officia! Velit odio ipsa illum molestiae laborum tenetur magni pariatur iure vero natus.',
                            content3: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates vitae deserunt laborum adipisci veniam nemo quas perspiciatis officia! Velit odio ipsa illum molestiae laborum tenetur magni pariatur iure vero natus.'
                        },
                        appearance: {
                            width: '100%',
                            padding: '1em',
                            backgroundColor: '#fff',
                            maxWidth: '960px',
                            margin: '0 auto',
                            fontFamily: 'sans-serif',
                            color: '#4d4d4d',
                            textAlign: 'center'
                        }
                    },
                    {
                        type: 'TwoColsBloc',
                        props: {
                            content1: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates vitae deserunt laborum adipisci veniam nemo quas perspiciatis officia! Velit odio ipsa illum molestiae laborum tenetur magni pariatur laborum iure vero natus.',
                            content2: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates vitae deserunt laborum adipisci veniam nemo quas perspiciatis officia! Velit odio ipsa illum molestiae laborum tenetur magni pariatur laborum iure vero natus.'
                        },
                        appearance: {
                            width: '100%',
                            padding: '1em',
                            backgroundColor: '#fff',
                            maxWidth: '960px',
                            margin: '0 auto',
                            fontFamily: 'sans-serif',
                            color: '#4d4d4d'
                        }
                    }
                ]
            } )
        },
        
        addToRows( context, { row } ) {
            context.commit( 'addRow', { row } )
        },
        
        deleteRow( context, row ) {
            context.commit( 'removeRow', row )
        },
        
        copyRow( context, row ) {
            context.commit( 'copyRow', row )
        },
        
        reorderRows( context, rowOrder ) {
            context.commit( 'reorderRows', rowOrder )
        },
        
        displayRte( context, rte ) {
            context.commit( 'showRte', rte )
        },
        
        bkgForm( context, value ) {
            context.commit( 'bkgForm', value )
        },
        
        updateRow( context, content ) {
            context.commit( 'updateRow', content )
        },
        
        updateColor( context, data ) {
            context.commit( 'updateColor', data )
        },
        
        updateBackground( context, data ) {
            context.commit( 'updateBackground', data )
        }
    }
} )
